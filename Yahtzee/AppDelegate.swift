//
//  AppDelegate.swift
//  Yahtzee
//
//  Created by Sam Ritchie on 16/08/2014.
//  Copyright (c) 2014 codesplice. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
                            
    var window: UIWindow?

}

