//
//  ViewController.swift
//  Yahtzee
//
//  Created by Sam Ritchie on 16/08/2014.
//  Copyright (c) 2014 codesplice. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
                            
    @IBOutlet var dice1: UIButton!
    @IBOutlet var dice2: UIButton!
    @IBOutlet var dice3: UIButton!
    @IBOutlet var dice4: UIButton!
    @IBOutlet var dice5: UIButton!
    @IBOutlet var categoryLabel: UILabel!
    @IBOutlet var scoreLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view
    }

    @IBAction func rerollOne(sender: AnyObject) {
        
    }
    
    @IBAction func rerollAll(sender: AnyObject) {
        
    }
}

